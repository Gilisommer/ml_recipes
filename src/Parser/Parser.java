package Parser;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;


//import org.apache.commons.lang3.math.Fraction;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;


public class Parser {

    final int sleepTimeBetweenRetries = 500; // in msec
    static final int retryCount = 3;

    public static ArrayList<String> GetSentencesFromMultipleRecipes() throws Exception
    {
        ArrayList<String> result = new ArrayList<>();
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/229289/zucchini-lasagna-with-beef-and-sausage/?internalSource=rotd&referringContentType=home%20page&clickId=cardslot%201"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/11721/tuna-macaroni-salad/?internalSource=popular&referringContentType=home%20page&clickId=cardslot%209"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/242406/chef-johns-cheese-blintzes/?internalSource=rr_recipe&referringId=254944&referringContentType=recipe&clickId=right%20rail%201&AnalyticsEvent=right%20rail%20nav"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/254944/chef-johns-basil-ricotta-gnocchi/?internalSource=hn_carousel%2002_Chef%20John%27s%20Basil%20Ricotta%20Gnocchi&referringId=16808&referringContentType=recipe%20hub&referringPosition=carousel%2002"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/13941/zucchini-patties/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%209"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/143809/best-steak-marinade-in-existence/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%208"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/25037/best-big-fat-chewy-chocolate-chip-cookie/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%209"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/10549/best-brownies/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%209"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/51535/fresh-southern-peach-cobbler/?internalSource=staff%20pick&referringContentType=home%20page&clickId=cardslot%2015"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/65155/blackberry-cobbler-ii/?internalSource=rr_recipe&referringId=51535&referringContentType=recipe&clickId=right%20rail%201&AnalyticsEvent=right%20rail%20nav"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/228126/caprese-salad-with-balsamic-reduction/?internalSource=staff%20pick&referringContentType=home%20page&clickId=cardslot%2011"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/233914/lemon-ricotta-pancakes-with-blueberry-sauce/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2016"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/16218/smothered-beef-short-ribs/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%209"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/135799/chicken-breast-cutlets-with-artichokes-and-capers/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2010"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/76089/kathys-easy-chile-chicken-and-rice/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2033"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/8524/chicken-breasts-with-herb-basting-sauce/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2018"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/15142/award-winning-peaches-and-cream-pie/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2048"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/246198/easy-french-toast-waffles/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2054"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/239871/toms-scrambled-egg-sandwich/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2058"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/232742/ice-cream-sandwich-cake/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%206"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/220987/baked-bbq-baby-back-ribs/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%202"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/244552/red-white-and-blueberry-grilled-chicken/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%209"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/241503/pina-colada-smoothie/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2013"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/219911/chef-johns-salmon/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2020"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/17303/taco-pie/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2014"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/254296/no-bake-peanut-butter-iceberg-pie/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2017"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/237065/chef-johns-spaghetti-al-tonno/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2018"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/75861/amazing-pork-tenderloin-in-the-slow-cooker/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2082"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/14057/yummy-veggie-omelet/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2088"));
//        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/52734/awesome-pasta-salad/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2085"));
        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/86587/scrambled-eggs-done-right/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2089"));
        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/44989/quick-and-easy-green-chile-chicken-enchilada-casserole/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2094"));
        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/240698/vermont-style-grilled-cheese/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2090"));
        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/77237/baked-omelet-squares/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2098"));
        result.addAll(GetSentencesFromRecipe("http://allrecipes.com/recipe/91597/original-homemade-italian-beef/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2097"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/chicken-tamale-pie-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/ina-garten/easy-tomato-soup-grilled-cheese-croutons-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/tortellini-with-pumpkin-alfredo-sauce-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/halibut-fish-tacos-with-cilantro-savoy-slaw-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/spicy-turkey-and-green-bean-stir-fry-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/tofu-reubens-with-salad-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/spaghetti-squash-and-meatballs-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/pork-chops-with-roasted-kale-and-walnut-pesto-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/pasta-with-roasted-broccoli-and-almond-tomato-sauce-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/robert-irvine/pepperoni-and-cheese-scrambled-eggs-recipe2.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/rachael-ray/scuderi-kids-fast-fake-baked-ziti-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/melissa-darabian/beef-and-black-bean-sliders-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/jeff-mauro/roasted-tomato-bisque-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/salmon-cakes-with-salad-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/giada-de-laurentiis/pizza-pot-pies-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/warm-chicken-and-butter-bean-salad-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/thai-chicken-with-carrot-ginger-salad-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/tilapia-with-escarole-and-lemon-pepper-oil-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/grilled-chicken-salad-with-gazpacho-dressing-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/salami-mozzarella-calzone-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/blueberry-cheesecake-galette.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/ree-drummond/ice-cream-freezer-pops-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/neapolitan-ice-cream-sandwich-cake-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/giada-de-laurentiis/lemon-ricotta-cookies-with-lemon-glaze-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/chocolate-peanut-butter-pudding-pie-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/blueberry-and-nectarine-cobbler-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/alton-brown/blueberry-buckle-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/boozy-cherry-chocolate-pies-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/healthy-no-bake-peanut-butter-cheesecake-bars.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/no-bake-healthy-strawberry-almond-cereal-bars.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/tiramisu-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/ina-garten/lemon-yogurt-cake-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/rhubarb-crisp-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/food-network-kitchens/banana-walnut-bread-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/maple-french-toast-and-bacon-cupcakes-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/ina-garten/greek-salad-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/quinoa-salad-recipe0.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/ina-garten/potato-salad-recipe.html"));
        result.addAll(GetSentencesFromRecipe("http://www.foodnetwork.com/recipes/giada-de-laurentiis/orzo-salad-recipe.html"));
        return result;
    }

    public static ArrayList<String> GetSentencesFromRecipe(String link) throws Exception
    {
        // get the text from the recipe link
        String fullText = ConnectAndExtractText(link);
        // replace each decimal point with a special character
        fullText = replaceDotBetweenNumbersToUnqStr(fullText);
        // remove shortcuts like gr.-->gr
        fullText = removeShortcuts(fullText);
        // split by dot
        return SplitToSentencesByDot(fullText);
    }

    //connect to recipe site and extract all text
    static public String ConnectAndExtractText(String link) throws Exception
    {
        Document doc = null;
        boolean isConnectionSuccessful = false;
        for (int i = 0; i < retryCount; i++)
        {
            try
            {
                doc = Jsoup.connect(link)  //  get desktop version of the site
                        .referrer("http://www.google.com")
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US;   rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6").get();
                isConnectionSuccessful = true;
                break;
            }
            catch (IOException e)
            {
                continue;
            }
        }
        if (isConnectionSuccessful)
        {
            return doc.body().text();
        }
        throw new Exception("Connection Error");
    }


    public static ArrayList<String> SplitToSentencesByDot(String text) {
        text = replaceDotBetweenNumbersToUnqStr(text).toLowerCase();
        String[] resultString = text.split("\\.");
        // bring back the dots
        ReplaceUnqStrWithDot(resultString);
        ArrayList<String> sentences = new ArrayList<String>(Arrays.asList(resultString));
        return sentences;
    }

    //prevent splitting amount by it decimal point
    private static String replaceDotBetweenNumbersToUnqStr(String text) {
        StringBuilder newText = new StringBuilder(text);

        for (int i = 1; i < text.length(); i++) {
            if (text.charAt(i) == '.') {
                if ((text.charAt(i - 1) >= '0') && (text.charAt(i - 1) <= '9')
                        && (text.charAt(i + 1) >= '0') && (text.charAt(i + 1) <= '9')) {
                    newText.setCharAt(i, '`');
                }
            }
        }
        return newText.toString();
    }

    private static void ReplaceUnqStrWithDot(String[] strings)
    {
        for (int i = 0; i < strings.length; i++)
        {
            strings[i] = strings[i].replace("`", ".");
        }
    }

    public static String[] splitSentenceToWords(String stringToSplit) {
        // lose "-", "," ....
        String cleanString = stringToSplit.replaceAll("[^A-Za-z0-9/.\\-% ]", "");
		//split by " " delimiter
        return cleanString.split("\\s+");
    }

    public static String[] PrepareSentenceForViterbi(String sentence)
    {
        while(sentence.charAt(0) == ' ')
        {
            sentence = sentence.substring(1,sentence.length());
        }
        sentence = sentence.replace(":","");
        sentence = sentence.replace("-"," ");
        //remove parentheses and string inside them

        //TODO: if there is a "," after the parentheses - attach the "," to the word before that
        sentence = sentence.replaceAll("\\(.*?\\) ?", "");
       return sentence.split(" ");
    }

    public static boolean isNumber(String word)
    {
        if (isNumeric(word) || isDouble(word) || (WordToNumber(word) != -1))
        {
            return true;
        }
        return false;
    }

    //Checks if the word represents a numeric value (hopefully an ingredient amount), such as: 5, 45, 9
    static boolean isNumeric(String word) {
        return word.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }


    //Checks if the word represents a fraction, such as: 1/2, 789/1235..

    static boolean isDouble(String word, String delimiter) {
        if (word.contains("\\" + delimiter) && (word.length() >= 3)) {
            String[] fraction = word.split("\\" + delimiter);
            if (isNumeric(fraction[0]) && isNumeric(fraction[1])) {
                return true;
            }
        }
        return false;
    }
    //checks if a word is amount (fraction) or just two words separated by "/" or "."
    static boolean isDouble(String word) {
        if (word.contains("/") && (word.length() >= 3)) {
            String[] fraction = word.split("/");
            if (isNumeric(fraction[0]) && isNumeric(fraction[1])) {
                return true;
            }
        }
        if (word.contains("\\.") && (word.length() >= 3)) {
            String[] fraction = word.split("\\.");
            if (isNumeric(fraction[0]) && isNumeric(fraction[1])) {
                return true;
            }
        }
        return false;
    }

    //create amount from fraction
    public static double parseAmount(String amount) {
        if (isNumeric(amount) || amount.contains("\\.")) {
            return Double.parseDouble(amount);
        } else {
            return parseRatio(amount);
        }
    }

    //create amount from fraction
    public static double parseRatio(String ratio) {
        if (ratio.contains("/")) {
            String[] rat = ratio.split("/");
            return Double.parseDouble(rat[0]) / Double.parseDouble(rat[1]);
        } else {
            return Double.parseDouble(ratio);
        }
    }

    //remove periods from shortcuts words
    public static String removeShortcuts(String text) {
        String[] SplitArray = text.split(" ");
        for (int i = 0; i < SplitArray.length; i++) {
            if (SplitArray[i].equals("tsp.")) {
                SplitArray[i] = "tsp";
            }
            if (SplitArray[i].equals("tbsp.")) {
                SplitArray[i] = "tbsp";
            }
            if (SplitArray[i].equals("lb.")) {
                SplitArray[i] = "lb";
            }
            if (SplitArray[i].equals("oz.")) {
                SplitArray[i] = "oz";
            }
            if (SplitArray[i].equals("g.")) {
                SplitArray[i] = "g";
            }
            if (SplitArray[i].equals("gr.")) {
                SplitArray[i] = "gr";
            }
            if (SplitArray[i].equals("kg.")) {
                SplitArray[i] = "kg";
            }
            if (SplitArray[i].equals("lbs.")) {
                SplitArray[i] = "lbs";
            }
            if (SplitArray[i].equals("lb.)")) {
                SplitArray[i] = "lb)";
            }
            if (SplitArray[i].equals("lb.),")) {
                SplitArray[i] = "lb),";
            }
            if (SplitArray[i].equals("lbs.)")) {
                SplitArray[i] = "lbs)";
            }
            if (SplitArray[i].equals("lbs.),")) {
                SplitArray[i] = "lbs),";
            }
            if (SplitArray[i].equals("tsp.)")) {
                SplitArray[i] = "tsp)";
            }
            if (SplitArray[i].equals("tsp.),")) {
                SplitArray[i] = "tsp),";
            }
            if (SplitArray[i].equals("tbsp.)")) {
                SplitArray[i] = "tbsp)";
            }
            if (SplitArray[i].equals("tbsp.),")) {
                SplitArray[i] = "tbsp),";
            }
            if (SplitArray[i].equals("lb.)")) {
                SplitArray[i] = "lb)";
            }
            if (SplitArray[i].equals("lb.),")) {
                SplitArray[i] = "lb),";
            }
            if (SplitArray[i].equals("oz.)")) {
                SplitArray[i] = "oz)";
            }
            if (SplitArray[i].equals("oz.),")) {
                SplitArray[i] = "oz),";
            }

        }
        StringBuilder builder = new StringBuilder();
        for (String s : SplitArray) {
            builder.append(s);
            builder.append(" ");
        }
        return builder.toString();
    }

    public static int WordToNumber(String word)
    {
        int number;
        switch (word)
        {
            case "one":
                number = 1;
                break;
            case "two":
                number = 2;
                break;
            case "three":
                number = 3;
                break;
            case "four":
                number = 4;
                break;
            case "five":
                number = 5;
                break;
            case "six":
                number = 6;
                break;
            case "seven":
                number = 7;
                break;
            case "eight":
                number = 8;
                break;
            case "nine":
                number = 9;
                break;
            case "ten":
                number = 10;
                break;
            case "eleven":
                number = 11;
                break;
            case "twelve":
                number = 12;
                break;
            case "thirteen":
                number = 13;
                break;
            case "fourteen":
                number = 14;
                break;
            case "fifteen":
                number = 15;
                break;
            case "sixteen":
                number = 16;
                break;
            case "seventeen":
                number = 17;
                break;
            case "eighteen":
                number = 18;
                break;
            case "nineteen":
                number = 19;
                break;
            case "twenty":
                number = 20;
                break;
            default:
                number = -1;
                break;
        }
        return number;
    }




}
