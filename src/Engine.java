import CRFPackage.*;
import Parser.Parser;
import SVM.SvmDataGenerator;
import SVM.SvmHandler;
import libsvm.svm_node;
import CRFPackage.CRFLabels.*;

import java.util.ArrayList;

public class Engine
{

    public Label[] GetAllIngredientsFromRecipe(String link)
    {
        // get ingredients section via SVM prediction
        String ingredientsSection = GetIngredientsSection(link);
        // get best labels via CRF model and Viterbi algorithm
        return GetBestLabelSequence(ingredientsSection);
    }

    /*get the ingredient section from a given URL*/
    private String GetIngredientsSection(String link)
    {
        // get sentences from link
        ArrayList<String> sentences = null;
        try
        {
            sentences = Parser.GetSentencesFromRecipe(link);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        // for each sentence check if it is the ingredient section
        for(String sentence : sentences)
        {
            boolean isIngredientSection = IsIngredientSection(sentence);
            if (isIngredientSection)
            {
                //System.out.println(sentence);
                return sentence;
            }
        }
        return null;
    }

    /*check if the passed sentence is a ingredient section*/
    private boolean IsIngredientSection(String sentence)
    {
        svm_node[] dataToPredict = null;
        try
        {
            dataToPredict = SvmDataGenerator.ConvertSentenceToSvmNodes(sentence);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        boolean result = false;
        try
        {
            result = SvmHandler.PredictSentence(dataToPredict);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }

    private Label[] GetBestLabelSequence(String sentence)
    {
        String[] wordsOfSentence = Parser.PrepareSentenceForViterbi(sentence);
        Viterbi viterbi = new Viterbi(wordsOfSentence, false);
        Label[] bestLabels = viterbi.GetBestLabelSequence();
        double score = viterbi.GetScore();
        ForwardAlgorithm fa = new ForwardAlgorithm(wordsOfSentence);
        double partitionFunctionValue = fa.GetPartitionFunctionValue();
        double result = score/partitionFunctionValue;
//        if(result < CRFLabels.CRFresultThreshold)
//        {
//            UserFeedback userFeedback = new UserFeedback(wordsOfSentence ,bestLabels );
//            userFeedback.GetUserFeedBack();
//        }
        System.out.println(result);
        //viterbi.PrintTable();
        //viterbi.PrintResult();
        return bestLabels;
    }

    public static void main(String [ ] args)
    {
        Engine engine = new Engine();
        String link1 = "http://allrecipes.com/recipe/21340/lindas-lasagna/?internalSource=hub%20recipe&referringId=17245&referringContentType=recipe%20hub&clickId=cardslot%2035";
        String link2 = "http://allrecipes.com/recipe/19389/greek-orzo-salad/?internalSource=hn_carousel%2001_Greek%20Orzo%20Salad&referringId=17548&referringContentType=recipe%20hub&referringPosition=carousel%2001";
        String link3 = "http://allrecipes.com/recipe/14315/sylvias-easy-greek-salad/?internalSource=rr_recipe&referringId=19389&referringContentType=recipe&clickId=right%20rail%208&AnalyticsEvent=right%20rail%20nav";
        String link4 = "http://allrecipes.com/recipe/141138/detroit-style-coney-dogs/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2014";
        String link5 = "http://allrecipes.com/recipe/240378/chili-dogs-with-cheese/?internalSource=rr_recipe&referringId=141138&referringContentType=recipe&clickId=right%20rail%205&AnalyticsEvent=right%20rail%20nav";
        String link6 = "http://allrecipes.com/recipe/223380/light-and-easy-greek-potato-salad/?internalSource=staff%20pick&referringContentType=home%20page&clickId=cardslot%207";
        String link7 = "http://allrecipes.com/recipe/11314/delicious-raspberry-oatmeal-cookie-bars/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%209";
        String link8 = "http://allrecipes.com/recipe/20144/banana-banana-bread/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%209";
        String link9 = "http://allrecipes.com/recipe/14756/filipino-ribs/?internalSource=previously%20viewed&referringContentType=home%20page&clickId=cardslot%2010";
        String link10 = "http://allrecipes.com/recipe/166601/whole-wheat-oatmeal-and-banana-pancakes/?internalSource=staff%20pick&referringContentType=home%20page&clickId=cardslot%2031";
        engine.GetAllIngredientsFromRecipe(link1);
        engine.GetAllIngredientsFromRecipe(link2);
        engine.GetAllIngredientsFromRecipe(link3);
        engine.GetAllIngredientsFromRecipe(link4);
        engine.GetAllIngredientsFromRecipe(link5);
        engine.GetAllIngredientsFromRecipe(link6);
        engine.GetAllIngredientsFromRecipe(link7);
        engine.GetAllIngredientsFromRecipe(link8);
        engine.GetAllIngredientsFromRecipe(link9);
        engine.GetAllIngredientsFromRecipe(link10);
    }
}
