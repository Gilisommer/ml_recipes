package SVM;

import Parser.Parser;

import java.io.*;
import java.util.*;

public class DictionaryFuncs
{
    static Dictionary<String,Integer> ourDictionary = new Hashtable<>();

    static public void CreateVocabularyFile()
    {
        ArrayList allRecipesSentences = null;
        try {
            allRecipesSentences = Parser.GetSentencesFromMultipleRecipes();
        } catch (Exception e) {
            e.printStackTrace();
        }
        SVM.DictionaryFuncs.initializeDictionary(allRecipesSentences);
        SVM.DictionaryFuncs.printDictToFile();
    }

    static public void initializeDictionary(ArrayList<String> sentences)
    {
        ourDictionary  = new Hashtable<>();
        int index=1;
        for(String sentence : sentences)
        {
            String[] wordsInSentence = Parser.splitSentenceToWords(sentence);
            for(String word : wordsInSentence)
            {
                if(ourDictionary.get(word)==null)//checks that the word isn't already in dict
                {
                    ourDictionary.put(word,index);
                    index++;
                }
            }
        }
    }
    public Dictionary<String,Integer> getDictionary()
    {
        return ourDictionary;
    }

    static public void printDictToFile()
    {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("ourDictionary.txt", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Enumeration<String> stringEnumeration = ourDictionary.keys();
        while(stringEnumeration.hasMoreElements())
        {
            writer.println(stringEnumeration.nextElement());
        }
        writer.close();
    }

    public static HashMap<String,Integer> LoadVocabularyFromFile()
    {
        HashMap<String,Integer> vocabulary = new HashMap<>();
        int index = 1;
        String vocabularyFileName = "ourDictionary.txt";
        try (BufferedReader br = new BufferedReader(new FileReader(vocabularyFileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                vocabulary.put(line,index);
                index++;
            }
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        return vocabulary;
    }
}
