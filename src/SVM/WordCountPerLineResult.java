
package SVM;
import java.util.HashMap;


public class WordCountPerLineResult
{
    public HashMap<Integer,Integer> m_indexCountMapResult; // used by ConvertSentenceToSvmNodes - for predicting
    public String m_lineToPrint; // used by CreateTrainingData - for training data

    WordCountPerLineResult(HashMap<Integer,Integer> indexCountMapResult, String lineToPrint)
    {
        m_indexCountMapResult = indexCountMapResult;
        m_lineToPrint = lineToPrint;
    }
}
