
package SVM;

import libsvm.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;

public class SvmHandler
{
    static String input_file_name = "trainingData.txt";
    static svm_problem prob;
    static svm_parameter param;
    static svm_model model = null;
    static String model_file_name = "svmModel";

    // read in a problem (in svmlight format) - load data to problem parameter
    private static void ReadProblem() throws IOException
    {
        BufferedReader fp = new BufferedReader(new FileReader(input_file_name));
        Vector<Double> vy = new Vector<>();
        Vector<svm_node[]> vx = new Vector<>();
        int max_index = 0;

        while(true)
        {
            String line = fp.readLine();
            if(line == null) break;

            StringTokenizer st = new StringTokenizer(line," \t\n\r\f:");

            vy.addElement(atof(st.nextToken()));
            int m = st.countTokens()/2;
            svm_node[] x = new svm_node[m];
            for(int j=0;j<m;j++)
            {
                x[j] = new svm_node();
                x[j].index = atoi(st.nextToken());
                x[j].value = atof(st.nextToken());
            }
            if(m>0) max_index = Math.max(max_index, x[m-1].index);
            vx.addElement(x);
        }

        prob = new svm_problem();
        prob.l = vy.size();
        prob.x = new svm_node[prob.l][];
        for(int i=0;i<prob.l;i++)
            prob.x[i] = vx.elementAt(i);
        prob.y = new double[prob.l];
        for(int i=0;i<prob.l;i++)
            prob.y[i] = vy.elementAt(i);

        if(param.gamma == 0 && max_index > 0)
            param.gamma = 1.0/max_index;

        if(param.kernel_type == svm_parameter.PRECOMPUTED)
            for(int i=0;i<prob.l;i++)
            {
                if (prob.x[i][0].index != 0)
                {
                    System.err.print("Wrong kernel matrix: first column must be 0:sample_serial_number\n");
                    System.exit(1);
                }
                if ((int)prob.x[i][0].value <= 0 || (int)prob.x[i][0].value > max_index)
                {
                    System.err.print("Wrong input format: sample_serial_number out of range\n");
                    System.exit(1);
                }
            }

        fp.close();
    }

    private static double atof(String s)
    {
        double d = Double.valueOf(s).doubleValue();
        if (Double.isNaN(d) || Double.isInfinite(d))
        {
            System.err.print("NaN or Infinity in input\n");
            System.exit(1);
        }
        return(d);
    }

    private static int atoi(String s)
    {
        return Integer.parseInt(s);
    }

    private static void SetParamValues()
    {
        param = new svm_parameter();
        // default values
        param.svm_type = svm_parameter.C_SVC;
        param.kernel_type = svm_parameter.RBF;
        param.degree = 3;//3
        param.gamma = 0;	// 1/num_features
        param.coef0 = 0;
        param.nu = 0.5;
        param.cache_size = 100;
        param.C = 1;//1
        param.eps = 1e-3;
        param.p = 0.1;
        param.shrinking = 1;
        param.probability = 0;
        param.nr_weight = 0;
        param.weight_label = new int[0];
        param.weight = new double[0];
    }

    private static void Train()
    {
        model = svm.svm_train(prob, param);
    }

    public static boolean PredictSentence(svm_node[] dataToPredict) throws Exception
    {
        if (model == null)
        {
            model = LoadModelFromFile();
        }
        double result = svm.svm_predict(model, dataToPredict);
        if (result < 0)
        {
            return false;
        }
        return true;
    }

    private static void SaveModelToFile() throws IOException
    {
        svm.svm_save_model(model_file_name, model);
    }

    private static svm_model LoadModelFromFile() throws Exception
    {
        return svm.svm_load_model(model_file_name);
    }

    public static void main(String [ ] args) throws Exception {
        SvmDataGenerator.CreateTrainingData();
        try
        {
            SetParamValues();
            ReadProblem();
            String error = svm.svm_check_parameter(prob,param);
            if(error != null)
            {
                System.out.println(error);
            }
            else
            {
                Train();
                SaveModelToFile();
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}

