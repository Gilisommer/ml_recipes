package SVM;
import Parser.Parser;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;
import libsvm.*;


public class SvmDataGenerator
{
    private static HashMap<String,Integer> vocabulary = null;

    public static void PrintSentencesCollectionToFile() throws Exception
    {
        ArrayList<String> sentencesCollection = Parser.GetSentencesFromMultipleRecipes();
        PrintWriter writer = new PrintWriter("sentencesToClassify.txt", "UTF-8");
        for (String sentence : sentencesCollection)
        {
            writer.println(sentence);
        }
        writer.close();
    }

    public static void CreateTrainingData() throws Exception
    {
        if (vocabulary == null)
            vocabulary = DictionaryFuncs.LoadVocabularyFromFile();
        String positiveLabel = "+1";
        String negativeLabel = "-1";

        PrintWriter trainingDataWriter = new PrintWriter("trainingData.txt", "UTF-8");
        BufferedReader br = new BufferedReader(new FileReader("sentencesClassified.txt"));
        String line;
        while ((line = br.readLine()) != null)
        {
            String trueLabel;
            if(line.trim().isEmpty()) continue;
            // in case this line was marked as ingredients line
            if(line.charAt(0) == '`')
            {
                line = line.replace("`","");
                trueLabel = positiveLabel;
            }
            // in case this line was not marked as ingredients line
            else
            {
                trueLabel = negativeLabel;
            }
            // lose extra space " "
            String newLine;
            if(line.charAt(0) == ' ')
            {
                newLine = line.substring(1,line.length());
            }
            else
            {
                newLine = line;
            }
            String lineToPrint = GenerateWordCountPerLine(newLine,trueLabel).m_lineToPrint;
            trainingDataWriter.println(lineToPrint);
        }
        trainingDataWriter.close();
    }

    public static svm_node[] ConvertSentenceToSvmNodes(String sentence) throws Exception
    {
        HashMap<Integer,Integer> indexCountMapResult = GenerateWordCountPerLine(sentence, null).m_indexCountMapResult;
        if(indexCountMapResult == null)
        {
            throw new Exception("indexCountMapResult is null");
        }
        // convert to SVM sample structure
        svm_node[] x = new svm_node[indexCountMapResult.size()];
        SortedSet<Integer> keys = new TreeSet<>(indexCountMapResult.keySet());
        int i = 0;
        for (Integer key : keys)
        {
            Integer wordCount = indexCountMapResult.get(key);
            svm_node svmNode = new svm_node();
            svmNode.index = key;
            svmNode.value = wordCount;
            x[i] = svmNode;
            i++;
        }
        return x;
    }


    private static WordCountPerLineResult GenerateWordCountPerLine(String line, String trueLabel)
    {
        String[] words = Parser.splitSentenceToWords(line);
        HashMap<String,Integer> wordCountMap = new HashMap<>();
        if (vocabulary == null)
            vocabulary = DictionaryFuncs.LoadVocabularyFromFile();
        // count the number of times a words appears in a line
        for (String word:words)
        {
            if (wordCountMap.containsKey(word))
            {
                wordCountMap.put(word, wordCountMap.get(word) + 1);
            }
            else
            {
                wordCountMap.put(word, 1);
            }
        }
        String lineToPrint = new String();
        if (trueLabel != null)
        {
            lineToPrint += trueLabel + " ";
        }
        HashMap<Integer,Integer> indexCountMap = new HashMap<>();
        // print the words count to file with their index in the vocabulary
        for (HashMap.Entry<String,Integer> entry : wordCountMap.entrySet())
        {
            String word = entry.getKey();
            if(vocabulary.containsKey(word))
            {
                int index = vocabulary.get(word);
                int wordCount = entry.getValue();
                indexCountMap.put(index, wordCount);
            }
        }

        SortedSet<Integer> keys = new TreeSet<>(indexCountMap.keySet());
        for (Integer key : keys)
        {
            Integer wordCount = indexCountMap.get(key);
            lineToPrint += key + ":" + wordCount + " ";
        }

        return new WordCountPerLineResult(indexCountMap, lineToPrint);
    }


}
