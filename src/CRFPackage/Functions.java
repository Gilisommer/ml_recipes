package CRFPackage;
import CRFPackage.CRFLabels.*;
import Parser.Parser;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class Functions {

    private static final int OCCURRENCE_THRESHOLD = 5;
    public IFeatureFunction[] featureFunctions ;
    public static HashMap<Integer, Float> weightMap;
    double negativeReturnValue = 0;
    double positiveReturnValue = 1;
    String weightsFileName = "Weights.txt";

    // MUST add every new feature function here in order for it's to be taken under consideration
    public Functions()
    {
        featureFunctions = new IFeatureFunction[]{
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f0(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f1(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f2(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f3(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f4(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f5(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f6(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f7(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f8(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f9(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f10(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f11(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f12(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f13(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f15(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f16(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f17(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f18(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f19(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f20(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f22(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f23(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f24(sentence, i, currentLabel, previousLabel);
                    }
                },
                new IFeatureFunction()
                {
                    public double featureFunction(String[] sentence, int i, Label currentLabel, Label previousLabel)
                    {
                        return f25(sentence, i, currentLabel, previousLabel);
                    }
                },
        };
    }
    List<String> typicalMeasureUnits = Arrays.asList("teaspoon", "cup", "tablespoon", "ml", "gr", "gallon", "pint",
            "liter", "stick", "oz", "lb", "clove", "package", "can", "pound", "envelope", "ounce", "bottle", "jar", "pinch", "bunch",
            "tbsp", "stalk", "slice", "link", "tsp", "sprig", "ml", "gram", "head", "mini", "container", "dash", "handful");

    // "bad" words that appear after numbers
    List<String> notAmountDescriptions = Arrays.asList("min", "minutes", "hour", "hours", "hr", "to", "servings", "degrees", "inch", "by");

    // tricky ingredients that come without measure units and amounts
    List<String> trickyIngredients = Arrays.asList("salt", "pepper", "black", "ground", "kosher");

    /**************************************************IRRELEVANT**************************************************/

    // if it's the first word of the sentence, and it's NOT a number - then it's probably IRRELEVANT
    public double f0(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == 0 && currentLabel == Label.IRRELEVANT && !Parser.isNumber(sentence[i]))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if the previous label is IRRELEVANT and the current word is NOT a number, then the current label is probably IRRELEVANT as well
    public double f1(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (previousLabel == Label.IRRELEVANT && !Parser.isNumber(sentence[i]) && currentLabel == Label.IRRELEVANT)
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if the current word is a number and:
    // the next word *is* a prep time ("min", "minutes", "hour", ...)  or "servings"
    // current label is "IRRELEVANT" --> it is probably true
    public double f5(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (currentLabel == Label.IRRELEVANT && Parser.isNumber(sentence[i]) && notAmountDescriptions.contains(sentence[i+1]))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // last word is probably IRRELEVANT
    public double f19(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == sentence.length - 1 && currentLabel == Label.IRRELEVANT)
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if the current word is "directions" then we are past the ingredients, and it's IRRELEVANT
    public double f3(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (sentence[i].contains("directions") && currentLabel == Label.IRRELEVANT)
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    /**************************************************AMOUNT**************************************************/

    // if this is the first word and it's a number - then it's probably an amount
    public double f2(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == 0 && isWordQualifiesAsAmount(sentence[i], currentLabel))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }


    // if the current word is a number and:
    // the next word is *not* a prep time ("min", "minutes", "hour", ...)  or "servings"
    // current label is "AMOUNT" --> it is probably true
    public double f4(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (isWordQualifiesAsAmount(sentence[i], currentLabel) && notAmountDescriptions.contains(sentence[i+1]) == false)
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }


    // if the current word is a number and the next word is a measure unit --> it is probably an AMOUNT
    public double f6(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (isWordQualifiesAsAmount(sentence[i], currentLabel) && isTypicalMeasureUnit(sentence[i+1]))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // 1 *1/2* case
    // if the previous word was a number, and the current word is also a number --> the current word is an AMOUNT as well
    public double f9(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == 0)
        {
            return negativeReturnValue;
        }
        if (isWordQualifiesAsAmount(sentence[i-1], previousLabel) && isWordQualifiesAsAmount(sentence[i], currentLabel))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // *1* 1/2 case
    // if the current word is a number, and the next word is a number --> the current word is an AMOUNT
    public double f10(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (sentence.length == i + 1)
        {
            return negativeReturnValue;
        }
        if (isWordQualifiesAsAmount(sentence[i], currentLabel) && Parser.isNumber(sentence[i+1]))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    /**************************************************MEASURE_UNIT**************************************************/

    // if the word contains one of the typical measure units --> it's probably a measure unit
    public double f7(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (isWordQualifiesAsMeasureUnit(sentence[i], currentLabel))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if the word contains one of the typical measure units and the word before that is labeled as an amount --> it's probably a measure unit
    public double f8(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == 0)
        {
            return negativeReturnValue;
        }
        if (isWordQualifiesAsMeasureUnit(sentence[i], currentLabel)&& isWordQualifiesAsAmount(sentence[i-1], previousLabel))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    /**************************************************INGREDIENT**************************************************/

    // 5 tomatoes
    // if the previous word is an amount and the current word is not a typical measure unit or a "bad" word --> INGREDIENT
    public double f11(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == 0)
        {
            return negativeReturnValue;
        }
        if (currentLabel == Label.INGREDIENT && isWordQualifiesAsAmount(sentence[i-1], previousLabel)
                && notAmountDescriptions.contains(sentence[i]) == false && isTypicalMeasureUnit(sentence[i]) == false)
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // 2 teaspoons sugar
    // if the previous word is a measure unit --> the current word is INGREDIENT
    public double f12(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == 0)
        {
            return negativeReturnValue;
        }
        if (isWordQualifiesAsMeasureUnit(sentence[i-1], previousLabel) && currentLabel == Label.INGREDIENT)
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if the previous word was an AMOUNT, INGREDIENT or MEASURE_UNIT, and the next word is probably a IRRELEVANT
    // this word is an INGREDIENT
    public double f14(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == 0 || sentence.length == i+1)
        {
            return negativeReturnValue;
        }
        if ((previousLabel == Label.INGREDIENT || isWordQualifiesAsMeasureUnit(sentence[i-1], previousLabel)
                ||isWordQualifiesAsAmount(sentence[i-1], previousLabel))
                && (GetNumberOfOccurrences(sentence[i+1], sentence) > OCCURRENCE_THRESHOLD) && currentLabel == Label.INGREDIENT)
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if this words ends with "," + the next word ends with "ed" + the previous word was an AMOUNT or MEASURE_UNIT or INGREDIENT
    // this word is an INGREDIENT
    public double f17(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == 0 || sentence.length == i+1)
        {
            return negativeReturnValue;
        }
        if ((isWordQualifiesAsAmount(sentence[i-1], previousLabel)|| previousLabel == Label.INGREDIENT ||
                isWordQualifiesAsMeasureUnit(sentence[i-1], previousLabel))
                && sentence[i].endsWith(",") && sentence[i+1].endsWith("ed") && currentLabel == Label.INGREDIENT)
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if the previous word was an INGREDIENT + current word is not a number or a measure unit
    // + there is an amount somewhere not far behind --> current is INGREDIENT
    public double f18(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (currentLabel == Label.INGREDIENT && previousLabel == Label.INGREDIENT && !Parser.isNumber(sentence[i])
                && !isTypicalMeasureUnit(sentence[i]) && AmountOrUnitNotFarBehind(sentence, i, 4))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    private boolean AmountOrUnitNotFarBehind(String[] sentence, int index, int range)
    {
        for (int i = 1; i <= range; i++)
        {
            if (index - i < 0)
            {
                return false;
            }
            if (Parser.isNumber(sentence[index-i]) || isTypicalMeasureUnit(sentence[index-i]))
            {
                return true;
            }
        }
        return false;
    }

    // check if current word is one of the tricky ingredients
    public double f24(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (currentLabel == Label.INGREDIENT && trickyIngredients.contains(sentence[i]))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    public double f25(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if(currentLabel == Label.INGREDIENT && AmountOrUnitNotFarBehind(sentence, i ,2))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }



    /**************************************************IRRELEVANT**************************************************/

    // if a word appears many times in the same sentence, and the previous label was an ingredient
    // and it's not a number or a measure unit
    // it is probably a IRRELEVANT (like "advertisement")
    public double f13(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (previousLabel == Label.INGREDIENT && currentLabel == Label.IRRELEVANT && !Parser.isNumber(sentence[i]) &&
                !isTypicalMeasureUnit(sentence[i]))
        {
            if (GetNumberOfOccurrences(sentence[i], sentence) > OCCURRENCE_THRESHOLD)
            {
                return positiveReturnValue;
            }
        }
        return negativeReturnValue;
    }

    // if the current word ends with an "ed", and the previous word was an ingredient --> current is IRRELEVANT
    public double f15(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (previousLabel == Label.INGREDIENT && currentLabel == Label.IRRELEVANT &&
                (sentence[i].endsWith("ed") || (sentence[i].endsWith("ly"))))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if the previous word was an ingredient and ended with a "," , this word is the beginning of a description (IRRELEVANT)
    public double f16(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == 0)
        {
            return negativeReturnValue;
        }
        if (previousLabel == Label.INGREDIENT && sentence[i-1].endsWith(",") && currentLabel == Label.IRRELEVANT)
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if the previous word was a IRRELEVANT and the current word is not an amount or a measure unit --> IRRELEVANT
    public double f20(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (previousLabel == Label.IRRELEVANT && currentLabel == Label.IRRELEVANT &&
                !Parser.isNumber(sentence[i]) && !isTypicalMeasureUnit(sentence[i]))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if the previous word was an ingredient and the current word is "or" then it's a IRRELEVANT
    public double f21(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (previousLabel == Label.INGREDIENT && currentLabel == Label.IRRELEVANT && (sentence[i].equals("or")))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if the previous word is a number and a IRRELEVANT and this word is a "bad" word --> IRRELEVANT
    public double f22(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == 0)
        {
            return negativeReturnValue;
        }
        if (previousLabel == Label.IRRELEVANT && currentLabel == Label.IRRELEVANT && Parser.isNumber(sentence[i-1])
        && (notAmountDescriptions.contains(sentence[i])))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }

    // if the current word is a number and the next word is a "bad" word --> IRRELEVANT
    public double f23(String[] sentence, int i, Label currentLabel, Label previousLabel)
    {
        if (i == sentence.length-1)
        {
            return negativeReturnValue;
        }
        if (currentLabel == Label.IRRELEVANT && Parser.isNumber(sentence[i]) && notAmountDescriptions.contains(sentence[i+1]))
        {
            return positiveReturnValue;
        }
        return negativeReturnValue;
    }


    /**************************************************AUX methods**************************************************/

    private int GetNumberOfOccurrences(String word, String[] sentence)
    {
        int count = 0;
        for (int i = 0; i < sentence.length; i++)
        {
            if (word.equals(sentence[i]))
            {
                count++;
            }
        }
        return count;
    }

    private boolean isTypicalMeasureUnit(String word)
    {
        // if a word ends with "ed" it is a verb - like "sliced", but it might contain "slice" which is a measure unit
        if (word.endsWith("ed"))
        {
            return false;
        }
        for (String unit : typicalMeasureUnits)
        {
            if (word.contains(unit) && (word.length() - unit.length() <= 2))
            {
                return true;
            }
        }
        return false;
    }

    private boolean isWordQualifiesAsAmount(String word, Label label)
    {
        if (label == Label.AMOUNT && Parser.isNumber(word))
        {
            return true;
        }
        return false;
    }

    private boolean isWordQualifiesAsMeasureUnit(String word, Label label)
    {
        if (label == Label.MEASURE_UNIT && isTypicalMeasureUnit(word))
        {
            return true;
        }
        return false;
    }


    public void InitializeWeightMap()
    {
        weightMap = new HashMap<>();
        for (int i = 0; i < featureFunctions.length; i++)
        {
            weightMap.put(i, new Float(0));
        }
    }


    public void LoadWeightsFromFile()
    {
        weightMap = new HashMap<>();
        int index = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(weightsFileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                weightMap.put(index, new Float(Double.parseDouble(line)));
                index++;
            }
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }

    public void SaveWeightsToFile()
    {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(weightsFileName, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < weightMap.size(); i++)
        {
            writer.println(weightMap.get(i));
        }
        writer.close();
    }

}
