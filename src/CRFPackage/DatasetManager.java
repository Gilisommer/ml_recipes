package CRFPackage;

import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class DatasetManager
{
    String crfDataSetFileName = "CrfDataset.txt";

    public ArrayList<Pair<String[], CRFLabels.Label[]>> GetTrainingSet() throws Exception
    {
        // read t.set file and initialize trainingSet data structure
        ArrayList<Pair<String[], CRFLabels.Label[]>> trainingSet = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(crfDataSetFileName)))
        {
            String line;
            int[] labelCounters = new int[CRFLabels.Label.values().length];
            while ((line = br.readLine()) != null)
            {
                String[] wordsAndLabels = line.split(" ");
                String[] sentence = new String[wordsAndLabels.length];
                CRFLabels.Label[] labels = new CRFLabels.Label[wordsAndLabels.length];
                int i = 0;
                for (String wordAndLabel : wordsAndLabels)
                {
                    String word = wordAndLabel.split(":")[0]; // get word
                    CRFLabels.Label label = StringToLabel(wordAndLabel.split(":")[1]); // get label
                    sentence[i] = word;
                    labels[i] = label;
                    i++;
                    labelCounters[label.ordinal()]++;
                }
                trainingSet.add(Pair.of(sentence, labels));
            }
            PrintLabelStatistics(labelCounters);
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        return trainingSet;
    }

    private void PrintLabelStatistics(int[] labelCounters)
    {
        int total = GetTotalNumberOfWords(labelCounters);
        System.out.println("TOTAL: " + total);
        for (CRFLabels.Label label : CRFLabels.Label.values())
        {
            System.out.println(label.toString() + ": " + (double)labelCounters[label.ordinal()]/(double)total);
        }
    }

    private int GetTotalNumberOfWords(int[] labelCounters)
    {
        int total = 0;
        for (int i = 0; i < labelCounters.length; i++)
        {
            total += labelCounters[i];
        }
        return total;
    }

    public static CRFLabels.Label StringToLabel(String str)
    {
        CRFLabels.Label label;
        switch (str)
        {
            case "AMOUNT":
                label = CRFLabels.Label.AMOUNT;
                break;
            case "MEASURE_UNIT":
                label = CRFLabels.Label.MEASURE_UNIT;
                break;
            case "INGREDIENT":
                label = CRFLabels.Label.INGREDIENT;
                break;
            case "IRRELEVANT":
                label = CRFLabels.Label.IRRELEVANT;
                break;
            default:
                label = null;
        }
        return label;
    }


    public static void main(String[] args)
    {
        DatasetManager dm = new DatasetManager();
        try
        {
            dm.GetTrainingSet();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
