package CRFPackage;

import org.apache.commons.lang3.tuple.Pair;
import CRFPackage.CRFLabels.*;

import java.util.ArrayList;

public class VotedPerceptron
{


    Functions functions;

    public VotedPerceptron()
    {
        functions = new Functions();
    }



    double SumFeatureFunctionOverSentence(String[] sentence, Label[] labels, IFeatureFunction f)
    {
        int sum = 0;
        for(int j = 0; j < sentence.length; j++)
        {
            if (j == 0)
            {
                sum += f.featureFunction(sentence, j, labels[j], null);
            }
            else
            {
                sum += f.featureFunction(sentence, j, labels[j], labels[j-1]);
            }

        }
        return sum;
    }

    public void Train(ArrayList<Pair<String[], CRFLabels.Label[]>> trainingSet)
    {
        DatasetManager datasetManager = new DatasetManager();
        Functions functions = new Functions();
        functions.InitializeWeightMap();
        for(Pair<String[], Label[]> pair : trainingSet)
        {
            String[] sentence = pair.getLeft();
            Viterbi viterbi = new Viterbi(sentence, true);
            Label[] trueLabels = pair.getRight();
            Label[] viterbiLabels = viterbi.GetBestLabelSequence();
            for (int i = 0; i < functions.featureFunctions.length; i++)
            {
                double oldWeight = Functions.weightMap.get(i);
                double newWeight = oldWeight + (SumFeatureFunctionOverSentence(sentence,trueLabels,functions.featureFunctions[i])
                        - SumFeatureFunctionOverSentence(sentence,viterbiLabels,functions.featureFunctions[i]));
                Functions.weightMap.put(i,new Float(newWeight));
            }
        }
        functions.SaveWeightsToFile();
    }

    public static void main(String[] args)
    {
        VotedPerceptron vp = new VotedPerceptron();
        DatasetManager dm = new DatasetManager();
        try {
            vp.Train(dm.GetTrainingSet());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }
}
