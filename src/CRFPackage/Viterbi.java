package CRFPackage;
import CRFPackage.CRFLabels.*;

/*class to find the best labels for the sentence */
public class Viterbi {
    private String[] sentence = null;
    private Label[] result;
    private Data[][] backtrackingTable = null;
    private Functions functions;
    private double bestScore = 0;
    private Container[][] secondBestPathTable = null;

    /*class to store the Data of the backtracking table*/
    private class Data
    {
        Label prevWordLabel;
        double score;

        public Data() {
            prevWordLabel = null;
            score = -Double.MAX_VALUE;
        }
    }

    private class Container
    {
        Data bestLabels;
        Data secondBestLabels;

        Container() {
            bestLabels = new Data();
            secondBestLabels = new Data();
        }
    }


    public Viterbi(String[] sentence, boolean isTrainingProcess)
    {
        functions = new Functions();
        if (!isTrainingProcess)
            functions.LoadWeightsFromFile();
        this.sentence = sentence;
        result = new Label[sentence.length];
        int numOfLabels = Label.values().length;
        int numOfWords = sentence.length;
        backtrackingTable = new Data[numOfLabels][numOfWords];
        for (int i = 0; i < numOfLabels; i++) {
            for (int j = 0; j < numOfWords; j++) {
                backtrackingTable[i][j] = new Data();
            }
        }
    }


    /*initialize the Data before starting the Viterbi algorithm*/
    public Label[] GetBestLabelSequence() {
        int numOfLabels = Label.values().length;
        int numOfWords = sentence.length;
        //initialize first column in backtracking table
        int wordIndex = 0;
        for (Label label : Label.values()) {
            backtrackingTable[label.ordinal()][0].score = GetScoreForWords(null, label, wordIndex, sentence);
            backtrackingTable[label.ordinal()][0].prevWordLabel = null;
        }
        CalculateLabels(numOfWords - 1);
        GetLabelsFromTable();
        return result;
    }

    /*recursive function to calculate the values of the backtracking table*/
    private void CalculateLabels(int wordIndex)
    {
        String word = sentence[wordIndex];
        if (wordIndex == 0) {
            return;
        }
        /*for wordIndex word and given label calculate the highest score and best label */
        for (Label currentLabel : Label.values()) {
            /*if the table entry for current word and label is empty calculate it*/
            if (backtrackingTable[currentLabel.ordinal()][wordIndex].score <= -Double.MAX_VALUE) {
                Double bestScore = null;
                Label bestPrevLabel = null;

                 /*for current word and label find the best label of previous word*/
                for (Label prevlabel : Label.values()) {
                    Double score = 0.0;
                    score += GetScoreForWords(prevlabel, currentLabel, wordIndex, sentence);
                    if (backtrackingTable[prevlabel.ordinal()][wordIndex - 1].score <= -Double.MAX_VALUE) {
                        CalculateLabels(wordIndex - 1);
                    }
                    score = score + backtrackingTable[prevlabel.ordinal()][wordIndex - 1].score;
                    if (bestScore == null) {
                        bestScore = score;
                        bestPrevLabel = prevlabel;
                    } else if (score > bestScore) {
                        bestScore = score;
                        bestPrevLabel = prevlabel;
                    }

                }
                backtrackingTable[currentLabel.ordinal()][wordIndex].score = bestScore;
                backtrackingTable[currentLabel.ordinal()][wordIndex].prevWordLabel = bestPrevLabel;
            } else {
                return;
            }
        }
    }

    public double GetScoreForWords(Label prevLabel, Label currentLabel, int wordIndex, String[] sentence)
    {
        double score = 0;
        for (int j = 0; j < functions.featureFunctions.length; j++) {
            double weightJ = Functions.weightMap.get(j);
            if (wordIndex == 0) {
                score += weightJ * functions.featureFunctions[j].featureFunction(sentence, wordIndex, currentLabel, null);
                continue;
            }
            score += weightJ * functions.featureFunctions[j].featureFunction(sentence, wordIndex, currentLabel, prevLabel);
        }
        return score;
    }

    private void GetLabelsFromTable() {
        if (backtrackingTable == null) {
            return;
        }
        Label lastLabel = null;
        Double score = null;
        //find best label for last word
        for (Label label_t : Label.values()) {
            if (score == null) {
                score = backtrackingTable[label_t.ordinal()][sentence.length - 1].score;
                lastLabel = backtrackingTable[label_t.ordinal()][sentence.length - 1].prevWordLabel;
            } else if (score < backtrackingTable[label_t.ordinal()][sentence.length - 1].score) {
                score = backtrackingTable[label_t.ordinal()][sentence.length - 1].score;
                lastLabel = backtrackingTable[label_t.ordinal()][sentence.length - 1].prevWordLabel;
            }
        }
        bestScore = score;
        result[sentence.length - 1] = lastLabel;
        //go backwards in table to find algorithm
        int wordIndex = sentence.length - 1;
        Label prevLabel = backtrackingTable[lastLabel.ordinal()][sentence.length - 1].prevWordLabel;
        wordIndex--;
        while (prevLabel != null) {
            result[wordIndex] = prevLabel;
            prevLabel = backtrackingTable[prevLabel.ordinal()][wordIndex].prevWordLabel;
            wordIndex--;
        }
    }

    public void PrintTable()
    {
        if (backtrackingTable == null || sentence == null) {
            return;
        }
        int rows = Label.values().length;
        int columns = sentence.length;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {

                System.out.format("Word:%s Score:%10f PrevLabel:%14s | ", sentence[j], backtrackingTable[i][j].score, backtrackingTable[i][j].prevWordLabel);
            }
            System.out.print("\n");
        }
    }

    public void PrintResult()
    {
        if (result == null || sentence == null) {
            return;
        }

        for (int i = 0; i < sentence.length; i++) {
            String word = "Word: " + sentence[i];
            String label = "Label: " + result[i];
            System.out.format("%5s %5s | ", word, label);
            System.out.println();
        }
        System.out.print("\n");

    }

    public double GetScore() {
        return bestScore;
    }

    /****************************************** function to calculate second best viterbi result **************************/
    public double GetSecondBestViterbiScore()
    {
        int numOfLabels = Label.values().length;
        int numOfWords = sentence.length;
        secondBestPathTable = new Container[numOfLabels][numOfWords];
        for (int i = 0; i < numOfLabels; i++) {
            for (int j = 0; j < numOfWords; j++) {
                secondBestPathTable[i][j] = new Container();
            }
        }
        S_initializeFirstColumn();
        S_calculateTableValues(sentence.length - 1);
        return S_getSecondBestScoer();
    }

    //initialize table's firs column
    private void S_initializeFirstColumn()
    {
        int numOfLabels = Label.values().length;
        int numOfWords = sentence.length;
        int wordIndex = 0;
        for (Label label : Label.values()) {
            secondBestPathTable[label.ordinal()][0].bestLabels.score = GetScoreForWords(null, label, wordIndex, sentence);
            secondBestPathTable[label.ordinal()][0].secondBestLabels.score = GetScoreForWords(null, label, wordIndex, sentence);
        }
    }

    private void S_calculateTableValues(int wordIndex)
    {
        if (wordIndex == 0) {
            return;
        }
        /*for word at index "wordIndex" and given label calculate the highest score and best label */
        for (Label currentLabel : Label.values()) {
            /*if the table entry for current word and label is empty calculate it*/
            if (secondBestPathTable[currentLabel.ordinal()][wordIndex].bestLabels.score <= -Double.MAX_VALUE) {
                double bestScore = -Double.MAX_VALUE;
                Label bestPrevLabel = null;
                Label secondBestPrevLabel = null;
                double secondBestScore = -Double.MAX_VALUE;
                 /*for current word and label find the best label of previous word*/
                for (Label prevlabel : Label.values()) {
                    Double score = 0.0;
                    score += GetScoreForWords(prevlabel, currentLabel, wordIndex, sentence);
                    //if the cell of previous word and previous label is empty calculate it
                    if (secondBestPathTable[prevlabel.ordinal()][wordIndex - 1].bestLabels.score <= -Double.MAX_VALUE) {
                        S_calculateTableValues(wordIndex - 1);
                    }
                    score = score + backtrackingTable[prevlabel.ordinal()][wordIndex - 1].score;
                    if (score > bestScore) {
                        secondBestScore = bestScore;
                        secondBestPrevLabel = bestPrevLabel;
                        bestScore = score;
                        bestPrevLabel = prevlabel;
                        continue;
                    } else if (score <= bestScore && score > secondBestScore) {
                        secondBestScore = score;
                        secondBestPrevLabel = prevlabel;
                        continue;
                    }
                }
                secondBestPathTable[currentLabel.ordinal()][wordIndex].bestLabels.score = bestScore;
                secondBestPathTable[currentLabel.ordinal()][wordIndex].bestLabels.prevWordLabel = bestPrevLabel;
                secondBestPathTable[currentLabel.ordinal()][wordIndex].secondBestLabels.score = secondBestScore;
                secondBestPathTable[currentLabel.ordinal()][wordIndex].secondBestLabels.prevWordLabel = secondBestPrevLabel;
            }
            else
            {
                return;
            }
        }
    }

    public double S_getBestScoer()
    {
        int numOfLabels = Label.values().length;
        int numOfWords = sentence.length;
        Double highestScore = null;
        for (int i = 0; i < numOfLabels; i++) {
            if (highestScore == null) {
                highestScore = secondBestPathTable[i][numOfWords - 1].bestLabels.score;
            }
            if (secondBestPathTable[i][numOfWords - 1].bestLabels.score > highestScore) {
                highestScore = secondBestPathTable[i][numOfWords - 1].bestLabels.score;
            }
        }
        return highestScore;
    }

    public double S_getSecondBestScoer()
    {
        int numOfLabels = Label.values().length;
        int numOfWords = sentence.length;
        Double highestScore = null;
        for (int i = 0; i < numOfLabels; i++) {
            if (highestScore == null) {
                highestScore = secondBestPathTable[i][numOfWords - 1].secondBestLabels.score;
            }
            if (secondBestPathTable[i][numOfWords - 1].secondBestLabels.score > highestScore) {
                highestScore = secondBestPathTable[i][numOfWords - 1].secondBestLabels.score;
            }
        }
        return highestScore;
    }

    public Label[] S_getLabelsFromTable()
    {
        Label[] secondBestResult = new Label[sentence.length];
        //create table of second best scores
        Data[][] secondBestDataTable = new Data[Label.values().length][sentence.length];
        for(int i = 0; i < Label.values().length; i++)
        {
            for(int j=0; j< sentence.length; j++)
            {
                secondBestDataTable[i][j] = new Data();
                secondBestDataTable[i][j] = secondBestPathTable[i][j].secondBestLabels;
            }
        }
        int rows = Label.values().length;
        int columns = sentence.length;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {

                System.out.format("Word:%s Score:%10f PrevLabel:%14s | ", sentence[j], secondBestDataTable[i][j].score, secondBestDataTable[i][j].prevWordLabel);
            }
            System.out.print("\n");
        }
        //find best score of last word in the table
        Double score = null;
        Label lastLabel = null;
        for (Label label_t : Label.values()) {
            if (score == null) {
                score = secondBestDataTable[label_t.ordinal()][sentence.length - 1].score;
                lastLabel = secondBestDataTable[label_t.ordinal()][sentence.length - 1].prevWordLabel;
            } else if (score < secondBestDataTable[label_t.ordinal()][sentence.length - 1].score) {
                score = secondBestDataTable[label_t.ordinal()][sentence.length - 1].score;
                lastLabel = secondBestDataTable[label_t.ordinal()][sentence.length - 1].prevWordLabel;
            }
        }
        secondBestResult[sentence.length - 1] = lastLabel;
        //go backwards in table to find algorithm
        int wordIndex = sentence.length - 1;
        Label prevLabel = secondBestDataTable[lastLabel.ordinal()][sentence.length - 1].prevWordLabel;
        wordIndex--;
        while (prevLabel != null) {
            secondBestResult[wordIndex] = prevLabel;
            prevLabel = secondBestDataTable[prevLabel.ordinal()][wordIndex].prevWordLabel;
            wordIndex--;
        }
        return secondBestResult;
    }

}