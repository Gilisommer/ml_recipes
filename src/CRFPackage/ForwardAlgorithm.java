package CRFPackage;
import CRFPackage.CRFLabels.*;


public class ForwardAlgorithm
{
    private String[] sentence = null;
    double result = 0;
    private Functions functions;
    double[][] backtrackingTable;

    public ForwardAlgorithm(String[] sentence)
    {
        functions = new Functions();
        functions.LoadWeightsFromFile();
        this.sentence = sentence;
        int numOfLabels = CRFLabels.Label.values().length;
        int numOfWords = sentence.length;
        backtrackingTable = new double[numOfLabels][numOfWords];
    }

    public double GetPartitionFunctionValue()
    {
        calculateTableValues();
        result = 0;
        calculateResult();
        return result;
    }

    private void calculateResult()
    {
        double score = 0;
        for(Label label : Label.values())
        {
            score = score + backtrackingTable[label.ordinal()][sentence.length -1];
        }
        result = score;
    }

    private void calculateTableValues()
    {
        //initialize first column (base case)
        int wordIndex = 0;
        for(Label label : Label.values())
        {
            backtrackingTable[label.ordinal()][0] = GetScoreForWords(null, label, wordIndex ,sentence);
        }
        //start calculating from second column
        wordIndex ++;
        while(wordIndex < sentence.length)
        {
            for (Label currentLabel : Label.values())
            {
                double score = 0;
                for(Label prevLabel : Label.values())
                {
                    //calculate sum over all previous labels
                    int prevIndex = wordIndex -1;
                    score = score + backtrackingTable[prevLabel.ordinal()][prevIndex];
                    score = score + GetScoreForWords(prevLabel,currentLabel,wordIndex,sentence);
                }
                backtrackingTable[currentLabel.ordinal()][wordIndex] = score;
            }
            wordIndex++;
        }

    }

    private double GetScoreForWords (Label prevLabel, Label currentLabel,int wordIndex, String[] sentence)
    {
        double score = 0;
        for (int j = 0; j < functions.featureFunctions.length; j++)
        {
            double weightJ = Functions.weightMap.get(j);
            if (wordIndex == 0)
            {
                score += weightJ * functions.featureFunctions[j].featureFunction(sentence, wordIndex, currentLabel, null);
                continue;
            }
            score += weightJ * functions.featureFunctions[j].featureFunction(sentence, wordIndex, currentLabel, prevLabel);
        }
        return score;
    }

    private void PrintTable()
    {
        if(backtrackingTable == null || sentence == null)
        {
            return;
        }
        int rows = Label.values().length;
        int columns = sentence.length;
        for(int i=0; i<rows; i++)
        {
            for (int j=0; j<columns; j++)
            {

                System.out.format(" Score:%10f | ",backtrackingTable[i][j] );
            }
            System.out.print("\n");
        }
    }


    public static void main(String[] args)
    {
        String string = "com feed a crowd best summer appetizers bottoms up summer drinks & cocktails cookout classics weekend picnic recipes prev recipe next recipe home recipes & how-tos tiramisu tiramisu recipe courtesy of food network kitchen print email tiramisu total time: 40 min prep: 30 min cook: 10 min yield: 8 servings level: easy total time: 40 min prep: 30 min cook: 10 min yield: 8 servings level: easy ingredients 6 large egg yolks 3/4 cup sugar 3/4 cup whole milk four 8-ounce containers mascarpone cheese, at room temperature 1 1/2 cups espresso or strong coffee, at room temperature 1/2 cup brandy or cognac 30 to 32 crisp italian ladyfingers (savoiardi) 1/4 cup dutch-process cocoa powder bittersweet chocolate, for shaving directions line an 8-inch-square baking dish with plastic wrap, leaving a 3-inch overhang on all sides";
        string = string.replaceAll("\\(.*?\\) ?", "");
        String [] sentence = string.split(" ");
        double result = 0;
        ForwardAlgorithm fa = new ForwardAlgorithm(sentence);
        result = fa.GetPartitionFunctionValue();
        System.out.println(result);
        fa.PrintTable();
    }

}
