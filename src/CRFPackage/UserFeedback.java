package CRFPackage;
import CRFPackage.CRFLabels.*;

import java.io.*;
import java.util.Arrays;
public class UserFeedback
{
    private String[] sentence;
    private Label[] labels;
    private Label[] userLabels;

    private enum userLabel
    {
        IR,
        A,
        MU,
        I,
        B
    }

    public UserFeedback (String[] sentence , Label[] labels)
    {
        this.sentence= sentence;
        this.labels = labels;
        userLabels = new Label[labels.length];
    }

    private void PrintInstruction()
    {
        System.out.println("The application is not sure the current labels are the correct ones");
        System.out.println("for each word pleas enter:");
        System.out.println("Ingredient = I\nAmount = A\nMeasure Unit = MU\nBridge = B\nIrrelevant = IR\n separate " +
                "each label with comma");
    }

    private void ReceiveFeedBack()
    {
        BufferedReader br = null;
        String input = null;
        try {

            br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println(Arrays.toString(sentence) + "\nEnter correct labels");
            input = br.readLine();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        String[] userInput = input.split(",");
        for (int i = 0; i<userInput.length; i++)
        {
            switch (userInput[i]){
                case "I":
                    userLabels[i] = Label.INGREDIENT;
                    break;
                case "MU":
                    userLabels[i] = Label.MEASURE_UNIT;
                    break;
                case "A":
                    userLabels[i] = Label.AMOUNT;
                    break;
                case "IR":
                    userLabels[i] = Label.IRRELEVANT;
                    break;
                default:
                    System.out.println("error");
                    return;
            }
        }
    }

    private void AddSentenceToTrainingData() throws IOException {
        String trainedSentence = "";
        for(int i=0; i<sentence.length; i++)
        {
            String labeledWord = "";
            if(i == sentence.length-1)
            {
                labeledWord = labeledWord+sentence[i] + ":" + userLabels[i].toString();
                continue;
            }
            else
            {
                labeledWord = labeledWord+sentence[i] + ":" + userLabels[i].toString()+ " ";
            }
            trainedSentence = trainedSentence + labeledWord;
        }
        System.out.println(trainedSentence);
        Writer output;
        output = new BufferedWriter(new FileWriter("CrfDataSet.txt", true));
        output.append(trainedSentence);
        output.close();
        //update number of user feedbacks
    }

    public void GetUserFeedBack()
    {
        PrintInstruction();
        ReceiveFeedBack();
    }




}
