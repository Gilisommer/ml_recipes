package CRFPackage;


public interface IFeatureFunction
{
    double featureFunction(String[] sentence, int i, CRFLabels.Label currentLabel, CRFLabels.Label previousLabel);
}