package CRFPackage;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import CRFPackage.CRFLabels.Label;

public class CrossValidation
{
    ArrayList<Pair<String[], Label[]>> originalTrainingSet;
    int originalTrainingDataSize;
    Group[] groups;

    public class Group
    {
        public ArrayList<Pair<String[], Label[]>> data;

        public Group()
        {
            data = new ArrayList<>();
        }
    }

    // k groups
    private void CreateFolds(int k)
    {
        DatasetManager datasetManager = new DatasetManager();
        ArrayList<Pair<String[], Label[]>> originalTrainingSet;
        try {
            originalTrainingSet = datasetManager.GetTrainingSet();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        originalTrainingDataSize = originalTrainingSet.size();
        int groupSize = originalTrainingSet.size()/k;
        groups = new Group[k];
        for (int i = 0; i < k; i++)
        {
            groups[i] = new Group();
            for (int j = 0; j < groupSize; j++)
            {
                int randomIndex = ThreadLocalRandom.current().nextInt(0, originalTrainingSet.size());
                groups[i].data.add(originalTrainingSet.get(randomIndex));
                originalTrainingSet.remove(randomIndex);
            }
        }
        if (!originalTrainingSet.isEmpty())
        {
            groups[k-1].data.addAll(originalTrainingSet);
        }
    }

    public void DoCrossValidation(int k)
    {
        double accuracySum = 0;
        CreateFolds(k);
        double[] sensitivityOverallAvg = new double[Label.values().length];
        double[] specificityOverallAvg = new double[Label.values().length];
        double[] accuracyAvgPerGroupArray = new double[k];
        for (int i = 0; i < k; i++)
        {
            System.out.println();
            System.out.println("*************** Group " + i + "***************");
            // group i will be the test set, all other groups will be training set
            ArrayList<Pair<String[], Label[]>> testSet = groups[i].data;
            ArrayList<Pair<String[], Label[]>> trainingSet = new ArrayList<>();
            for (int j = 0; j < k; j++)
            {
                if (i == j) continue;
                trainingSet.addAll(groups[j].data);
            }
            // train
            VotedPerceptron vp = new VotedPerceptron();
            vp.Train(trainingSet);
            // test
            double[] avgSensitivityPerLabel = new double[Label.values().length];
            double[] avgSpecificityPerLabel = new double[Label.values().length];

            accuracySum=0;
            for (Pair<String[], Label[]> pair : testSet)
            {
                String[] sentence = pair.getLeft();
                Label[] trueLabels = pair.getRight();
                Viterbi viterbi = new Viterbi(sentence, false);
                Label[] viterbiLabels = viterbi.GetBestLabelSequence();
                for (Label label : Label.values())
                {
                    avgSensitivityPerLabel[label.ordinal()] += GetSentenceSensitivityForLabel(label, trueLabels, viterbiLabels);
                    avgSpecificityPerLabel[label.ordinal()] += GetSentenceSpecificityForLabel(label, trueLabels, viterbiLabels);
                }
                int mismatchCount = GetMismatchCount(trueLabels, viterbiLabels);
                double accuracy = 1.0 - (double)mismatchCount/(double)sentence.length;
                accuracySum += accuracy;

            }
            accuracyAvgPerGroupArray[i] = accuracySum/testSet.size();
            avgSensitivityPerLabel = GetDividedArrayByNum(avgSensitivityPerLabel, testSet.size());
            avgSpecificityPerLabel = GetDividedArrayByNum(avgSpecificityPerLabel, testSet.size());
            PrintSensitivityArray(avgSensitivityPerLabel);
            System.out.println();
            PrintSpecificityArray(avgSpecificityPerLabel);
            // update overall average
            sensitivityOverallAvg = UpdateAvgOverall(sensitivityOverallAvg, avgSensitivityPerLabel);
            specificityOverallAvg = UpdateAvgOverall(specificityOverallAvg, avgSpecificityPerLabel);
            System.out.println("-----Accuaracy avarage-----");
            System.out.println(accuracyAvgPerGroupArray[i]);
        }
        // transform to averages
        sensitivityOverallAvg = GetDividedArrayByNum(sensitivityOverallAvg, k);
        specificityOverallAvg = GetDividedArrayByNum(specificityOverallAvg, k);
        System.out.println("~~~~~~~~~Averages Overall~~~~~~~~~");
        PrintSensitivityArray(sensitivityOverallAvg);
        PrintSpecificityArray(specificityOverallAvg);
    }

    private void PrintSensitivityArray(double[] avgSensitivityPerLabel)
    {
        System.out.println("------Sensitivity Average Array------");
        for (int i = 0 ; i < Label.values().length; i++)
        {
            System.out.println(Label.values()[i].toString() + ": " + avgSensitivityPerLabel[i]);
        }
    }

    private void PrintSpecificityArray(double[] avgSpecificityPerLabel)
    {
        System.out.println("------Specificity Average Array------");
        for (int i = 0 ; i < Label.values().length; i++)
        {
            System.out.println(Label.values()[i].toString() + ": " + avgSpecificityPerLabel[i]);
        }
    }

    private int GetMismatchCount(Label[] trueLabels, Label[] viterbiLabels)
    {
        int mismatchCount = 0;
        for (int i = 0; i < trueLabels.length; i++)
        {
            if (trueLabels[i] != viterbiLabels[i])
            {
                mismatchCount++;
            }
        }
        return mismatchCount;
    }

    private double GetSentenceSensitivityForLabel(Label label, Label[] trueLabels, Label[] viterbiLabels)
    {
        int truePositives = 0;
        int totalOfLabel = 0;
        for (int i = 0; i < trueLabels.length; i++)
        {
            if (trueLabels[i] == label)
            {
                totalOfLabel++;
                if (viterbiLabels[i] == label)
                {
                    truePositives++;
                }
            }
        }
        if (totalOfLabel == 0)
            return 0;
        double sensitivity = (double)truePositives/(double)totalOfLabel;
        return sensitivity;
    }

    private double GetSentenceSpecificityForLabel(Label label, Label[] trueLabels, Label[] viterbiLabels)
    {
        int trueNegatives = 0;
        int totalOfNotLabel = 0;
        for (int i = 0; i < trueLabels.length; i++)
        {
            if (trueLabels[i] != label)
            {
                totalOfNotLabel++;
                if (viterbiLabels[i] != label)
                {
                    trueNegatives++;
                }
            }
        }
        if (totalOfNotLabel == 0)
            return 0;
        double specificity = (double)trueNegatives/(double)totalOfNotLabel;
        return specificity;
    }

    private double[] GetDividedArrayByNum(double[] array, double num)
    {
        double[] dividedArray = new double[array.length];
        for (int i = 0; i < array.length; i++)
        {
            dividedArray[i] = array[i]/num;
        }
        return dividedArray;
    }

    private double[] UpdateAvgOverall(double[] current, double[] newValues)
    {
        double[] updatedValues = new double[current.length];
        for (int i = 0; i < updatedValues.length; i++)
        {
            updatedValues[i] = current[i] + newValues[i];
        }
        return updatedValues;
    }




    public static void main(String [] args)
    {
        CrossValidation cv = new CrossValidation();
        cv.DoCrossValidation(10);
    }

}